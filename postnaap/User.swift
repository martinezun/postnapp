//
//  User.swift
//  postnaap
//
//  Created by Nivaldo Martinez on 4/30/19.
//  Copyright © 2019 Transportation America. All rights reserved.
//

import Foundation

struct User {
    var id: Int?
    var name: String?
    var username: String?
    var email: String?
}
