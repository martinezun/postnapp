//
//  UserManaged+Mapping.swift
//  postnaap
//
//  Created by Nivaldo Martinez on 4/30/19.
//  Copyright © 2019 Transportation America. All rights reserved.
//

import Foundation

extension UserManaged {
    func mappedObject() -> User {
        return User(id: Int(self.id), name: self.name, username: self.username, email: self.email)
    }
}
