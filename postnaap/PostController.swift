//
//  PostController.swift
//  postnaap
//
//  Created by Nivaldo Martinez on 4/30/19.
//  Copyright © 2019 Transportation America. All rights reserved.
//

import UIKit
import Kingfisher

class PostController: UIViewController {
    
    var post: Post?
    let dataProvider = LocalService()
    var comments = [Comment]()
    let cellId = "commentCell"

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descTextView: UITextView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userEmailLabel: UILabel!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var commentsTableView: UITableView!
    @IBOutlet weak var favImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getComments()
        commentsTableView.rowHeight = UITableView.automaticDimension
        commentsTableView.estimatedRowHeight = 400
        favImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(toogleFavorite)))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureProfilePic()
        
        if let post = post {
            titleLabel.text = post.title
            descTextView.text = post.body
            setFavImage(post.favorite!)
            dataProvider.getUserBy(id: post.userId!) { (user) in
                if let user = user  {
                    self.userNameLabel.text = user.name
                    self.userEmailLabel.text = user.email
                    let avatarUrl = "https://api.adorable.io/avatars/140/\(user.username ?? "def").png"
                    self.profileImg.kf.setImage(with: URL(string: avatarUrl)!, placeholder: UIImage(named: "user_shape"))
                }
            }
        } else {
            let alert = UIAlertController(title: "Error", message: "No se encontró el post", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { (_) in
                self.navigationController?.popViewController(animated: true)
            }))
            present(alert, animated: true, completion: nil)
        }
    }
    
    
    fileprivate func configureProfilePic() {
        profileImg.layer.cornerRadius = profileImg.frame.height / 2
        profileImg.layer.masksToBounds = true
    }
    
    fileprivate func getComments() {
        if let post = post, let id = post.id {
            dataProvider.getCommentsByPost(id: id) { (comments) in
                if let comments = comments {
                    self.comments = comments
                    self.commentsTableView.reloadData()
                }
            }
        }
    }
    
    @objc func toogleFavorite() {
        if let post = post, let fav = post.favorite {
            dataProvider.toogleFavorite(post, favorite: !fav)
            setFavImage(!fav)
        }
    }
    
    fileprivate func setFavImage(_ fav: Bool) {
        favImageView.image = fav ? #imageLiteral(resourceName: "fav") : #imageLiteral(resourceName: "no_fav")
    }
}

extension PostController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        let comment = comments[indexPath.row]
        cell.textLabel?.text = comment.email
        cell.detailTextLabel?.text = comment.body
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
}
