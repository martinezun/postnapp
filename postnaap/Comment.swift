//
//  Coment.swift
//  postnaap
//
//  Created by Nivaldo Martinez on 4/30/19.
//  Copyright © 2019 Transportation America. All rights reserved.
//

import Foundation

struct Comment {
    var postId: Int?
    var id: Int?
    var email: String?
    var name: String?
    var body: String?
}
