//
//  Post.swift
//  postnaap
//
//  Created by Nivaldo Martinez on 4/29/19.
//  Copyright © 2019 Transportation America. All rights reserved.
//

import SwiftyJSON

struct Post {
    var userId: Int?
    var id: Int?
    var title: String?
    var body: String?
    var viewed: Bool?
    var favorite: Bool?
    var order: Int?
}
