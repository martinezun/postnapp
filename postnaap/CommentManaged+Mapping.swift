//
//  CommentManaged.swift
//  postnaap
//
//  Created by Nivaldo Martinez on 4/30/19.
//  Copyright © 2019 Transportation America. All rights reserved.
//

import Foundation

extension CommentManaged {
    
    func mappedObject() -> Comment {
        return Comment(postId: Int(self.postId), id: Int(self.id), email: self.email, name: self.name, body: self.body)
    }
}
