//
//  PostCellTableViewCell.swift
//  postnaap
//
//  Created by Nivaldo Martinez on 4/29/19.
//  Copyright © 2019 Transportation America. All rights reserved.
//

import UIKit

class PostCellTableViewCell: UITableViewCell {
    
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var viewedView: UIView!
    @IBOutlet weak var favImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(with post: Post) {
        bodyLabel.text = post.body
        viewedView.isHidden = post.viewed ?? true
        favImageView.isHidden = !post.favorite! || !post.viewed!
    }

}
