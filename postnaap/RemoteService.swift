//
//  ServeManager.swift
//  postnaap
//
//  Created by Nivaldo Martinez on 4/29/19.
//  Copyright © 2019 Transportation America. All rights reserved.
//

import Alamofire
import SwiftyJSON

class RemoteService {
    
    let baseUrl = "https://jsonplaceholder.typicode.com/"
    
    func getPosts(callBack: @escaping ([JSON]?) -> Swift.Void) {
        Alamofire.request("\(baseUrl)posts", method: .get).responseJSON { (response) in
            switch(response.result) {
                case .success(let data):
                    let entries = JSON(data).arrayValue
                    callBack(entries)
                case .failure(let error):
                    callBack(nil)
                    print(error.localizedDescription)
            }
        }
    }
    
    func getUserBy(id: Int, callBack: @escaping (JSON?) -> Swift.Void) {
        Alamofire.request("\(baseUrl)users/\(id)", method: .get).responseJSON { (response) in
            switch(response.result) {
            case .success(let data):
                let entry = JSON(data)
                callBack(entry)
            case .failure(let error):
                callBack(nil)
                print(error.localizedDescription)
            }
        }
    }
    
    func getCommentsByPost(id: Int, callBack: @escaping ([JSON]?) -> Swift.Void) {
        Alamofire.request("\(baseUrl)posts/\(id)/comments", method: .get).responseJSON { (response) in
            switch(response.result) {
            case .success(let data):
                let entries = JSON(data).arrayValue
                callBack(entries)
            case .failure(let error):
                callBack(nil)
                print(error.localizedDescription)
            }
        }
    }
}
