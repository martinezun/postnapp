//
//  ViewController.swift
//  postnaap
//
//  Created by Nivaldo Martinez on 4/29/19.
//  Copyright © 2019 Transportation America. All rights reserved.
//

import UIKit

class PostsController: UIViewController {
    
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    let cellId = "postCell"
    let dataProvider = LocalService()
    
    var posts = [Post]()
    var selectedPost: Post?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 400
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        retrieveData()
    }

    @IBAction func resetData(_ sender: Any) {
        let alert = UIAlertController(title: "Warning", message: "¿Quieres resetear toda la infomación?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Si", style: .destructive, handler: { (_) in
            self.dataProvider.deleteAllPosts()
            self.retrieveData()
        }))
        alert.addAction(UIAlertAction(title: "No", style: .destructive, handler: nil))
        present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func deleteAllPosts(_ sender: Any) {
        if (posts.isEmpty) {
            let alert = UIAlertController(title: "Error", message: "No hay nada que borrar", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { (_) in
                self.navigationController?.popViewController(animated: true)
            }))
            present(alert, animated: true, completion: nil)
        } else {
            let sheet = UIAlertController(title: "¿Seguro que quieres eliminar todos los post?", message: "Esta acción no se puede revertir", preferredStyle: .actionSheet)
            sheet.addAction(UIAlertAction(title: "Eliminar", style: .destructive, handler: { (_) in
                self.dataProvider.softDeleteAllPost()
                self.posts.removeAll()
                self.tableView.reloadData()
            }))
            sheet.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
            present(sheet, animated: true, completion: nil)
        }
    }
    
    @IBAction func didChangeSegment(_ sender: Any) {
        tableView.setContentOffset(.zero, animated: false)
        retrieveData()
    }
    
    fileprivate func retrieveData() {
        if segment.selectedSegmentIndex == 0 {
            getPosts()
        } else {
            getFavorites()
        }
    }
    /**
     This get all post from json api
    */
    fileprivate func getPosts() {
        dataProvider.getPosts(completionHandler: { (posts) in
            if let posts = posts {
                self.posts = posts
                self.tableView.reloadData()
            }
        })
    }
    
    fileprivate func getFavorites() {
        dataProvider.getPosts(favorite: true, completionHandler: { (posts) in
            if let posts = posts {
                self.posts = posts
                self.tableView.reloadData()
            }
        })
    }
    
    fileprivate func deletePost(_ indexPath: IndexPath) {
        let sheet = UIAlertController(title: "¿Seguro que quieres eliminar este post?", message: "Esta acción no se puede revertir", preferredStyle: .actionSheet)
        sheet.addAction(UIAlertAction(title: "Eliminar", style: .destructive, handler: { (_) in
            self.dataProvider.softDeletePost(self.posts[indexPath.row])
            self.posts.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .left)
        }))
        sheet.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        present(sheet, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? PostController {
            controller.post = selectedPost
        }
    }
}

extension PostsController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! PostCellTableViewCell
        let post = posts[indexPath.row]
        cell.configure(with: post)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let post = posts[indexPath.row]
        dataProvider.markAsViewedPost(post)
        selectedPost = post
        performSegue(withIdentifier: "showPostSegue", sender: self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteBtn = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            self.deletePost(indexPath)
        }
        
        return [deleteBtn]
    }
}

