//
//  LocalService.swift
//  postnaap
//
//  Created by Nivaldo Martinez on 4/29/19.
//  Copyright © 2019 Transportation America. All rights reserved.
//

import CoreData
import SwiftyJSON

class LocalService {
    fileprivate let remoteService = RemoteService()
    fileprivate let stack = CoreDataStack.shared
    
    func getPosts(favorite: Bool = false, completionHandler: @escaping ([Post]?) -> Swift.Void) {
        
        completionHandler(queryPosts(favorite: favorite))
        
        remoteService.getPosts { (posts) in
            if let posts = posts {
                
                var order = 1
                for postDic in posts {
                    if let _ = self.getPostBy(id: postDic["id"].intValue) {
                        
                    } else {
                        self.insert(postDic, order: order)
                    }
                    
                    order += 1
                }
                
                completionHandler(self.queryPosts(favorite: favorite))
            } else {
                completionHandler(nil)
            }
        }
    }
    
   fileprivate func queryPosts(favorite: Bool = false) -> [Post]? {
        let context = stack.persistentContainer.viewContext
        let request: NSFetchRequest<PostManaged> = PostManaged.fetchRequest()
        
        let sort = NSSortDescriptor(key: "order", ascending: true)
        request.sortDescriptors = [sort]
        
        let predicate = NSPredicate(format: "\(favorite ? "favorite = \(favorite) and" : "") active = \(true)")
        request.predicate = predicate
        
        do {
            let fetchedPosts = try context.fetch(request)
            return fetchedPosts.map({$0.mappedObject()})
        } catch {
            print("Error fetching local posts")
            return nil
        }
    }
    
    fileprivate func getPostBy(id: Int) -> PostManaged? {
        let context = stack.persistentContainer.viewContext
        let request: NSFetchRequest<PostManaged> = PostManaged.fetchRequest()
        
        let sort = NSSortDescriptor(key: "order", ascending: true)
        request.sortDescriptors = [sort]
        
        let predicate = NSPredicate(format: "id = \(id)")
        request.predicate = predicate
        
        do {
            let fetchedPosts = try context.fetch(request)
            if fetchedPosts.count > 0 {
                return fetchedPosts.last
            } else {
                return nil
            }
        } catch {
            print("Error fetching local post by id")
            return nil
        }
    }
    
    fileprivate func insert(_ dic: JSON, order: Int) {
        let context = stack.persistentContainer.viewContext
        let post = PostManaged(context: context)
        
        post.id = Int16(dic["id"].intValue)
        post.userId = Int16(dic["userId"].intValue)
        post.title = dic["title"].stringValue
        post.body = dic["body"].stringValue
        post.viewed = order > 20
        post.order = Int16(order)
        
        do {
            try context.save()
        } catch  {
            print("error update")
        }
    }
    
    func markAsViewedPost(_ post: Post) {
        let context = stack.persistentContainer.viewContext
        if let postManaged = getPostBy(id: post.id!) {
            postManaged.viewed = true
            
            do {
                try context.save()
            } catch {
                print("Error update Viewed")
            }
        }
    }
    
    func toogleFavorite(_ post: Post, favorite: Bool) {
        let context = stack.persistentContainer.viewContext
        if let postManaged = getPostBy(id: post.id!) {
            postManaged.favorite = favorite
            
            do {
                try context.save()
            } catch {
                print("Error update Viewed")
            }
        }
    }
    
    func softDeletePost(_ post: Post) {
        let context = stack.persistentContainer.viewContext
        if let postManaged = getPostBy(id: post.id!) {
            postManaged.active = false
            
            do {
                try context.save()
            } catch {
                print("Error deleting post")
            }
        }
    }
    
    func softDeleteAllPost() {
        let context = stack.persistentContainer.viewContext
        let request: NSFetchRequest<PostManaged> = PostManaged.fetchRequest()
        
        do {
            let fetchedPosts = try context.fetch(request)
            for post in fetchedPosts {
                post.active = false
            }
            
            try context.save()
        } catch {
            print("Error deleting all posts")
        }
    }
    
    /// User
    func getUserBy(id: Int, completionHandler: @escaping (User?) -> Swift.Void) {
        if let user = queryUserBy(id: id) {
            completionHandler(user.mappedObject())
        } else {
            remoteService.getUserBy(id: id) { (jsonUser) in
                if let jsonUser = jsonUser {
                    self.inserUser(jsonUser)
                    completionHandler(self.queryUserBy(id: id)?.mappedObject())
                }
            }
        }
    }
    
    fileprivate func queryUserBy(id: Int) -> UserManaged? {
        let context = stack.persistentContainer.viewContext
        let request: NSFetchRequest<UserManaged> = UserManaged.fetchRequest()
        
        let predicate = NSPredicate(format: "id = \(id)")
        request.predicate = predicate
        
        do {
            let fetchedPosts = try context.fetch(request)
            if fetchedPosts.count > 0 {
                return fetchedPosts.last
            } else {
                return nil
            }
        } catch {
            print("Error fetching local post by id")
            return nil
        }
    }
    
    fileprivate func inserUser(_ json: JSON) {
        let context = stack.persistentContainer.viewContext
        let user = UserManaged(context: context)
        
        user.id = Int16(json["id"].intValue)
        user.email = json["email"].stringValue
        user.username = json["username"].stringValue
        user.name = json["name"].stringValue
        
        do {
            try context.save()
        } catch  {
            print("error update")
        }
    }
    
    /// COMMENTS
    
    func getCommentsByPost(id: Int, completionHandler: @escaping ([Comment]?) -> Swift.Void) {
        remoteService.getCommentsByPost(id: id) { (comments) in
            if let comments = comments {
                var result = [Comment]()
                for comment in comments {
                    let c = Comment(postId: comment["postId"].intValue, id: comment["id"].intValue, email: comment["email"].stringValue, name: comment["name"].stringValue, body: comment["body"].stringValue)
                    result.append(c)
                }
                
                completionHandler(result)
            } else {
                completionHandler(nil)
            }
        }
    }
    
    /// WARNING
    
    func deleteAllPosts() {
        let context = stack.persistentContainer.viewContext
        let request: NSFetchRequest<PostManaged> = PostManaged.fetchRequest()
        
        do {
            let fetchedPosts = try context.fetch(request)
            for post in fetchedPosts {
                context.delete(post)
            }
        } catch {
            print("Error fetching local posts")
        }
    }
}
