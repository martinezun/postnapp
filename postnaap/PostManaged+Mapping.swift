//
//  PostManaged+Mapping.swift
//  postnaap
//
//  Created by Nivaldo Martinez on 4/29/19.
//  Copyright © 2019 Transportation America. All rights reserved.
//

import Foundation

extension PostManaged {
    func mappedObject() -> Post {
        return Post(userId: Int(self.userId), id: Int(self.id), title: self.title, body: self.body, viewed: self.viewed, favorite: self.favorite, order: Int(self.order))
    }
}
