###Build

- requisitos para instalar la app:
	- Xcode 10+
	- Swift 5
> No es necesario instalar las librerías porque estas estan ya en el repositorio.

- la aplicación necesita una conexión a internet la primera vez para cargar los posts desde la API.

###Librerias Utilizadas

- Alamofire: permite consumir servicios web de manera elegante y facil.
- SwiftyJSON: permite manejar objetos JSON de forma facil, con datos opcionales y no opcionales.
- Kingfisher: para manejo y cacheo de imagenes. En está aplicación se usa principalmente para cargar imagenes desde una url y cachearla.